FROM docker.io/node:16.17.1-buster-slim

# PeerTube image for OpenShift Origin

ARG DO_UPGRADE=
ENV DEBIAN_FRONTEND=noninteractive \
    HOME=/app \
    NODE_CONFIG_DIR=/app/config \
    NODE_ENV=production \
    PATH="/app/.node-live/lib/node_modules/.bin:/app/.node-live/bin:$PATH" \
    PT_REPO=https://github.com/Chocobozzz/PeerTube \
    PT_VERSION=4.3.0

LABEL io.k8s.description="PeerTube is a NodeJS VoD solution." \
      io.k8s.display-name="PeerTube $PT_VERSION" \
      io.openshift.expose-services="9000:http" \
      io.openshift.tags="peertube" \
      io.openshift.non-scalable="false" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-peertube" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="$PT_VERSION"

USER root

COPY config/* /

RUN set -ex \
    && rm -rf /var/lib/apt/lists/* \
    && apt-get -y update \
    && if test "$DO_UPGRADE"; then \
	echo "# Upgrade Base Image"; \
	apt-get -y upgrade; \
	apt-get -y dist-upgrade; \
    fi \
    && if test "$DEBUG"; then \
	echo "# Install Debugging Tools" \
	&& apt-get -y install vim; \
    fi \
    && echo "# Install PeerTube dependencies" \
    && apt-get -y --no-install-recommends install libnss-wrapper ldap-utils \
	ca-certificates git gnupg openssl postgresql-client ffmpeg \
	libmime-tools-perl python curl xz-utils \
    && mkdir -p /app \
    && echo "# Install PeerTube $PT_VERSION" \
    && curl -fsL -o /usr/src/peertube.tar.xz \
	"$PT_REPO/releases/download/v$PT_VERSION/peertube-v$PT_VERSION.tar.xz" \
    && tar -C /app --strip-components=1 -xf /usr/src/peertube.tar.xz \
    && ( \
	cd /app \
	&& yarn config set network-timeout 600000 \
	&& for retry in one too tri; \
	    do \
		NODE_ENV=production yarn install --pure-lockfile --production \
		&& break; \
		if test "$retry" = tri; then \
		    echo CRITICAL: failed installing dependencies; \
		    exit 1; \
		fi; \
	    done \
	&& yarn cache clean; \
    ) \
    && mkdir -p /app/storage/tmp /app/storage/avatars /app/storage/videos \
	/app/storage/streaming-playlists /app/storage/redundancy \
	/app/storage/logs /app/storage/previews /app/storage/thumbnails \
	/app/storage/torrents /app/storage/captions /app/storage/cache \
	/app/storage/plugins /app/storage/client-overrides \
	/app/storage/.config \
    && mv /nsswrapper.sh /reset-tls.sh /myldapsearch /usr/local/bin/ \
    && rm -fr /app/.config \
    && ln -sf /app/storage/.config /app/ \
    && echo "# Fixing permissions" \
    && for i in /etc/ssl /usr/local/share/ca-certificates /app/storage \
	    /app/config /app/.npm /app/.cache; \
	do \
	    mkdir -p "$i" \
	    && chown -R 1001:root "$i" \
	    && chmod -R g=u "$i"; \
	done \
    && chown 1001:root /app \
    && chmod g=u /app \
    && ( chmod g=u -R /run /tmp || echo nevermind ) \
    && echo "# Cleaning up" \
    && apt-get -y remove --purge gnupg git xz-utils curl \
    && apt-get purge -y --auto-remove -o \
	APT::AutoRemove::RecommendsImportant=false \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /usr/share/doc /usr/share/man /app/.git* \
	/backend.conf /webserver.conf /nginx-status.conf \
	/usr/src/peertube.tar.xz \
    && unset HTTP_PROXY HTTPS_PROXY NO_PROXY DO_UPGRADE http_proxy https_proxy

WORKDIR /app
CMD [ "npm", "start" ]
ENTRYPOINT [ "/run-peertube.sh" ]
USER 1001

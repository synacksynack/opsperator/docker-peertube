#!/bin/sh

if test "$DEBUG"; then
    set -x
    DBG_LEVEL=debug
else
    DBG_LEVEL=info
fi
. /usr/local/bin/nsswrapper.sh
. /usr/local/bin/reset-tls.sh

cpt=0
DO_LIVE=${DO_LIVE:-true}
LIVE_TRANSCODE_THREADS=${LIVE_TRANSCODE_THREADS:-2}
OPENLDAP_BIND_DN_PREFIX="${OPENLDAP_BIND_DN_PREFIX:-cn=peertube,ou=services}"
OPENLDAP_BIND_PW="${OPENLDAP_BIND_PW:-secret}"
OPENLDAP_DOMAIN=${OPENLDAP_DOMAIN:-demo.local}
OPENLDAP_HOST=${OPENLDAP_HOST:-}
OPENLDAP_PROTO=${OPENLDAP_PROTO:-ldap}
OPENLDAP_TLS_VERIFY=false
PEERTUBE_ADMIN_PASSWORD=${PEERTUBE_ADMIN_PASSWORD:-yayayayayayayayayas3cR3742}
PEERTUBE_ALLOW_CONFIG_EDITS=${PEERTUBE_ALLOW_CONFIG_EDITS:-false}
PEERTUBE_CACHE_MINVIEWS=${PEERTUBE_CACHE_MINVIEWS:-10}
PEERTUBE_CACHE_SIZE=${PEERTUBE_CACHE_SIZE:-10GB}
PEERTUBE_CACHE_STRATEGY=${PEERTUBE_CACHE_STRATEGY:-recently-added}
PEERTUBE_CACHE_TTLMIN="${PEERTUBE_CACHE_TTLMIN:-48 hours}"
PEERTUBE_HAS_NSFW=${PEERTUBE_HAS_NSFW:-true}
PEERTUBE_NAME="${PEERTUBE_NAME:-PeerTube}"
PEERTUBE_NSFW_POLICY=${PEERTUBE_NSFW_POLICY:-do_not_list}
POSTGRES_DB=${POSTGRES_DB:-peertube}
POSTGRES_HOST=${POSTGRES_HOST:-127.0.0.1}
POSTGRES_PASSWORD="${POSTGRES_PASSWORD:-secret}"
POSTGRES_PORT=${POSTGRES_PORT:-5432}
POSTGRES_USER=${POSTGRES_USER:-peertube}
POSTGRES_SSL=${POSTGRES_SSL:-false}
PUBLIC_PROTO=${PUBLIC_PROTO:-http}
PT_VERSION=${PT_VERSION:-master}
REDIS_DB=${REDIS_DB:-0}
REDIS_HOST=${REDIS_HOST:-127.0.0.1}
REDIS_PORT=${REDIS_PORT:-6379}
RTMP_PORT=${RTMP_PORT:-1935}
RTMPS_CERT_FILE=${RTMPS_CERT_FILE:-}
RTMPS_KEY_FILE=${RTMPS_KEY_FILE:-}
RTMPS_PORT=${RTMPS_PORT:-1936}
SMTP_HOST=${SMTP_HOST:-127.0.0.1}
SMTP_PORT=${SMTP_PORT:-25}
if test -z "$LIVE_MAX_DURATION"; then
    LIVE_MAX_DURATION=-1
fi
if test -z "$LIVE_INSTANCE_MAX"; then
    LIVE_INSTANCE_MAX=-1
fi
if test -z "$LIVE_USER_MAX"; then
    LIVE_USER_MAX=-1
fi
if test -z "$OPENLDAP_BASE"; then
    OPENLDAP_BASE=`echo "dc=$OPENLDAP_DOMAIN" | sed 's|\.|,dc=|g'`
fi
if test -z "$OPENLDAP_PORT" -a "$OPENLDAP_PROTO" = ldaps; then
    OPENLDAP_PORT=636
elif test -z "$OPENLDAP_PORT"; then
    OPENLDAP_PORT=389
fi
if test -z "$PEERTUBE_DOMAIN"; then
    PEERTUBE_DOMAIN=peertube.$OPENLDAP_DOMAIN
fi
if test "$PUBLIC_PROTO" = https; then
    PEERTUBE_HTTPS=true
    PEERTUBE_PORT=443
else
    PEERTUBE_HTTPS=false
    PEERTUBE_PORT=80
fi
if test -z "$PEERTUBE_DESC_SHORT"; then
    PEERTUBE_DESC_SHORT='PeerTube, a federated (ActivityPub) video streaming platform using P2P (BitTorrent) directly in the web browser with WebTorrent and Angular.'
fi
if test -z "$PEERTUBE_DESC"; then
    PEERTUBE_DESC='Welcome to this PeerTube instance'
fi
if test -z "$PEERTUBE_COC"; then
    PEERTUBE_COC="Don't be evil"
fi
if test -z "$PEERTUBE_TERMS"; then
    PEERTUBE_TERMS='No terms for now'
fi
if test -z "$PEERTUBE_TRUST_PROXY"; then
    PEERTUBE_TRUST_PROXY="10.0.0.0/8 192.168.0.0/16 172.16.0.0/12"
fi
if ! test "$PEERTUBE_ALLOW_CONFIG_EDITS" = true; then
    PEERTUBE_ALLOW_CONFIG_EDITS=false
fi
if ! test "$DO_LIVE" = true; then
    DO_LIVE=false
fi
if test "$RTMPS_CERT_FILE" -a "$RTMPS_KEY_FILE"; then
    DO_RTMP_TLS=true
else
    DO_RTMP_TLS=false
fi
if test "$REPORT_URI_ADDRESS"; then
    DO_REPORT_URI=true
else
    DO_REPORT_URI=false
    REPORT_URI_ADDRESS=
fi
if test -z "$SMTP_TLS" -a "$SMTP_PORT" = 465; then
    SMTP_TLS=true
    SMTP_NO_STARTTLS=true
elif test -z "$SMTP_TLS"; then
    SMTP_TLS=false
fi
if test -z "$SMTP_NO_STARTTLS"; then
    SMTP_NO_STARTTLS=true
fi
if $SMTP_TLS; then
    if test -s /certs/ca.crt -a -z "$SMTP_CA"; then
	SMTP_CA=/certs/ca.crt
    fi
fi
if test -z "$SMTP_CA"; then
    SMTP_CA=null
else
    SMTP_CA="'$SMTP_CA'"
fi

echo Waiting for Postgres backend ...
while true
do
    if echo '\d' | PGPASSWORD="$POSTGRES_PASSWORD" \
	    psql -U "$POSTGRES_USER" -h "$POSTGRES_HOST" \
	    -p "$POSTGRES_PORT" "$POSTGRES_DB" >/dev/null 2>&1; then
	echo " Postgres is alive!"
	break
    elif test "$cpt" -gt 25; then
	echo "Could not reach Postgres" >&2
	exit 1
    fi
    sleep 5
    echo Postgres is ... KO
    cpt=`expr $cpt + 1`
done
if test "$OPENLDAP_HOST"; then
    echo Waiting for LDAP backend ...
    while true
    do
	if LDAPTLS_REQCERT=never \
		ldapsearch -H $OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT \
		-D "$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE" \
		-b "ou=users,$OPENLDAP_BASE" \
		-w "$OPENLDAP_BIND_PW" \
		uid >/dev/null 2>&1; then
	    echo " LDAP is alive!"
	    break
	elif test "$cpt" -gt 25; then
	    echo Could not reach OpenLDAP >&2
	    exit 1
	fi
	sleep 5
	echo LDAP is ... KO
	cpt=`expr $cpt + 1`
    done
    if test "$OPENLDAP_PROTO" = ldaps; then
	if ldapsearch -H $OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT \
		-D "$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE" \
		-b "ou=users,$OPENLDAP_BASE" \
		-w "$OPENLDAP_BIND_PW" \
		uid >/dev/null 2>&1; then
	    if test -s /etc/ldap/ldap.conf; then
		LDAPCAFILE=`awk '/TLS_CACERT/{print $2}' /etc/ldap/ldap.conf`
	    fi
	    if test -z "$LDAPCAFILE"; then
		if test -s /etc/pki/tls/cert.pem; then
		    LDAPCAFILE=/etc/pki/tls/cert.pem
		elif test -s /etc/ssl/certs/ca-certificates.crt; then
		    LDAPCAFILE=/etc/ssl/certs/ca-certificates.crt
		fi
	    fi
	fi
    fi
fi
if test "$LDAPCAFILE" -a -s "$LDAPCAFILE"; then
    OPENLDAP_TLS_VERIFY=true
    export LDAP_CUSTOM_CA_PATH=$LDAPCAFILE
fi
export OPENLDAP_TLS_VERIFY

for d in tmp avatars videos streaming-playlists redundancy logs previews \
    thumbnails torrents captions cache plugins client-overrides .config bin
do
    if ! test -d /app/storage/$d; then
	mkdir -p /app/storage/$d
    fi
done

if test -s $NODE_CONFIG_DIR/config.yaml; then
    CVERS=`awk -F= '/^# VERSION=/{print $2}' $NODE_CONFIG_DIR/config.yaml | cut -d= -f1`
    test -z "$CVERS" && CVERS=undefined
    if ! test "$CVERS" = "$PT_VERSION"; then
	mv $NODE_CONFIG_DIR/config.yaml \
	    $NODE_CONFIG_DIR/config.yaml.$CVERS-$(date +%s)
    fi
fi

if ! test -s $NODE_CONFIG_DIR/config.yaml; then
    sed -e "s|DB_NAME|$POSTGRES_DB|" \
	-e "s|DB_HOST|$POSTGRES_HOST|" \
	-e "s|DO_LIVE|$DO_LIVE|" \
	-e "s|DO_RTMP_TLS|$DO_RTMP_TLS|" \
	-e "s|DB_PASS|$POSTGRES_PASSWORD|" \
	-e "s|DB_PORT|$POSTGRES_PORT|" \
	-e "s|DO_REPORTURI|$DO_REPORT_URI|" \
	-e "s|DB_USER|$POSTGRES_USER|" \
	-e "s|DB_SSL|$POSTGRES_SSL|" \
	-e "s|DBG_LEVEL|$DBG_LEVEL|" \
	-e "s|CACHE_SIZE|$PEERTUBE_CACHE_SIZE|" \
	-e "s|CACHE_STRATEGY|$PEERTUBE_CACHE_STRATEGY|" \
	-e "s|CACHE_TTLMIN|$PEERTUBE_CACHE_TTLMIN|" \
	-e "s|CACHE_MINVIEWS|$PEERTUBE_CACHE_MINVIEWS|" \
	-e "s|LIVE_INSTANCE_MAX|$LIVE_INSTANCE_MAX|" \
	-e "s|LIVE_MAX_DURATION|$LIVE_MAX_DURATION|" \
	-e "s|LIVE_USER_MAX|$LIVE_USER_MAX|" \
	-e "s|LIVE_TRANSCODE_THREADS|$LIVE_TRANSCODE_THREADS|" \
	-e "s|PT_COC|$PEERTUBE_COC|" \
	-e "s|PT_DESC|$PEERTUBE_DESC|" \
	-e "s|PT_DO_NSFW|$PEERTUBE_HAS_NSFW|" \
	-e "s|PT_FQDN|$PEERTUBE_DOMAIN|" \
	-e "s|PT_HTTPS|$PEERTUBE_HTTPS|" \
	-e "s|PT_NAME|$PEERTUBE_NAME|" \
	-e "s|PT_ALLOW_EDITS|$PEERTUBE_ALLOW_CONFIG_EDITS|" \
	-e "s|PT_NSFW_POLICY|$PEERTUBE_NSFW_POLICY|" \
	-e "s|PT_PORT|$PEERTUBE_PORT|" \
	-e "s|PT_SHORTDESC|$PEERTUBE_DESC_SHORT|" \
	-e "s|PT_TERMS|$PEERTUBE_TERMS|" \
	-e "s|PT_VERSION|$PT_VERSION|" \
	-e "s|RD_HOST|$REDIS_HOST|" \
	-e "s|RD_DBID|$REDIS_DB|" \
	-e "s|RD_PORT|$REDIS_PORT|" \
	-e "s|RPURI_ADDRESS|$REPORT_URI_ADDRESS|" \
	-e "s|RTMP_PORT|$RTMP_PORT|" \
	-e "s|RTMPS_PORT|$RTMPS_PORT|" \
	-e "s|RTMPS_CERT_FILE|$RTMPS_CERT_FILE|" \
	-e "s|RTMPS_KEY_FILE|$RTMPS_KEY_FILE|" \
	-e "s|SMTP_CA|$SMTP_CA|" \
	-e "s|SMTP_HOST|$SMTP_HOST|" \
	-e "s|SMTP_NO_STARTTLS|$SMTP_NO_STARTTLS|" \
	-e "s|SMTP_PORT|$SMTP_PORT|" \
	-e "s|SMTP_TLS|$SMTP_TLS|" \
	/config.yaml >"$NODE_CONFIG_DIR/config.yaml"

    for entry in $PEERTUBE_TRUST_PROXY
    do
	cat <<EOF
- '$entry'
EOF
    done >>"$NODE_CONFIG_DIR/config.yaml"

    cat "$NODE_CONFIG_DIR/config.yaml" >"$NODE_CONFIG_DIR/$NODE_ENV.yaml"
    echo "NOTICE: Done generating site configuration ($PT_VERSION)"
fi

if ! test -s "$NODE_CONFIG_DIR/.admin-pw"; then
    export PT_INITIAL_ROOT_PASSWORD="$PEERTUBE_ADMIN_PASSWORD"
    echo "$PEERTUBE_ADMIN_PASSWORD" >"$NODE_CONFIG_DIR/.admin-pw"
else
    unset PT_INITIAL_ROOT_PASSWORD
fi

/provision.sh &

if test "$DEBUG"; then
    echo DEBUG: Runtime configuration
    cat "$NODE_CONFIG_DIR/config.yaml"
fi

unset CA_CHECK ONLY_TRUST_KUBE_CA OPENLDAP_BASE OPENLDAP_DOMAIN OPENLDAP_HOST \
    OPENLDAP_PORT OPENLDAP_PROTO POSTGRES_DB POSTGRES_HOST POSTGRES_PASSWORD \
    POSTGRES_PORT POSTGRES_USER POSTGRES_SSL PUBLIC_PROTO REDIS_DATABASE \
    REDIS_HOST REDIS_PORT SMTP_HOST SMTP_NO_STARTTLS SMTP_PORT SMTP_TLS \
    PEERTUBE_CACHE_SIZE PEERTUBE_CACHE_STRATEGY PEERTUBE_CACHE_TTLMIN \
    PEERTUBE_COC PEERTUBE_DESC PEERTUBE_DESC_SHORT PEERTUBE_DOMAIN \
    PEERTUBE_HAS_NSFW PEERTUBE_NAME PEERTUBE_NSFW_POLICY PEERTUBE_TERMS \
    PEERTUBE_TRUST_PROXY REPORT_URI_ADDRESS PEERTUBE_HTTPS PEERTUBE_PORT \
    DO_REPORT_URI LDAPCAFILE OPENLDAP_TLS_VERIFY

exec $@

#!/bin/sh

if test "$DEBUG"; then
    set -x
fi

while ! bash -c "echo 'GET /' >/dev/tcp/127.0.0.1/9000" >/dev/null 2>&1
do
    echo Waiting for PeerTube to startup
    sleep 10
done
sleep 20

if ! test -s "$NODE_CONFIG_DIR/.admin-pw"; then
    if ! echo "$PEERTUBE_ADMIN_PASSWORD" \
	    | node ./dist/scripts/reset-password.js -u root; then
	echo WARNING: Failed initializing root password
    else
	echo "$PEERTUBE_ADMIN_PASSWORD" >"$NODE_CONFIG_DIR/.admin-pw"
    fi
elif ! grep "^$PEERTUBE_ADMIN_PASSWORD$" "$NODE_CONFIG_DIR/.admin-pw" >/dev/null; then
    if ! echo "$PEERTUBE_ADMIN_PASSWORD" \
	    | node ./dist/scripts/reset-password.js -u root; then
	echo WARNING: Failed resetting root password
    else
	echo "$PEERTUBE_ADMIN_PASSWORD" >"$NODE_CONFIG_DIR/.admin-pw"
    fi
fi

if test "$http_proxy$HTTP_PROXY"; then
    if test "$HTTP_PROXY"; then
	export http_proxy=$HTTP_PROXY
    else
	export HTTP_PROXY=$http_proxy
    fi
    npm config set http-proxy $http_proxy
    npm config set https-proxy $http_proxy
    npm config set proxy $http_proxy
    yarn config set http-proxy $http_proxy
    yarn config set https-proxy $http_proxy
    yarn config set proxy $http_proxy
fi

LEMON_PROTO=${LEMON_PROTO:-http}
POSTGRES_DB=${POSTGRES_DB:-peertube}
POSTGRES_HOST=${POSTGRES_HOST:-127.0.0.1}
POSTGRES_PASSWORD="${POSTGRES_PASSWORD:-secret}"
POSTGRES_PORT=${POSTGRES_PORT:-5432}
POSTGRES_USER=${POSTGRES_USER:-peertube}
TODAY=$(date +%Y-%m-%d)
SHOULD_RELOAD=false
if test "$DO_CHAPTERS_PLUGIN"; then
    if ! test -d /app/storage/plugins/node_modules/peertube-plugin-chapters; then
	npm run plugin:install -- --npm-name peertube-plugin-chapters
	SHOULD_RELOAD=true
    fi
fi
if test "$OIDC_SECRET"; then
    if ! test -d /app/storage/plugins/node_modules/peertube-plugin-auth-openid-connect; then
	npm run plugin:install -- --npm-name peertube-plugin-auth-openid-connect
	SHOULD_RELOAD=true
    fi
    HASID=`echo "SELECT id FROM plugin WHERE name = 'auth-openid-connect'" | PGPASSWORD="$POSTGRES_PASSWORD" psql -U "$POSTGRES_USER" -h "$POSTGRES_HOST" -p "$POSTGRES_PORT" "$POSTGRES_DB" | awk '/^[ \t]*[1-9]/{print $1}'`
    OIDC_CLIENT_ID=${OIDC_CLIENT_ID:-peertube}
    OIDC_DN=${OIDC_DN:-}
    OIDC_MAIL=${OIDC_MAIL:-email}
    test -z "$OIDC_SCOPE" && OIDC_SCOPE='openid email profile'
    OIDC_USERNAME=${OIDC_USERNAME:-preferred_username}
    if test -z "$LEMON_HOST"; then
	LEMON_HOST=auth.$OPENLDAP_DOMAIN
    fi
    if test "$HASID"; then
	cat <<EOF
UPDATE plugin SET "updatedAt" = '$TODAY 00:00:00.000+00', settings =
      '{"auth-display-name":"LLNG-OIDC","discover-url":"$LEMON_PROTO://$LEMON_HOST/.well-known/openid-configuration","client-id":"$OIDC_CLIENT_ID","client-secret":"$OIDC_SECRET","scope":"$OIDC_SCOPE","username-property":"$OIDC_USERNAME","mail-property":"$OIDC_MAIL","display-name-property":"$OIDC_DN","role-property":""}'
    WHERE name = 'auth-openid-connect'
EOF
    else
	cat <<EOF
INSERT INTO plugin (name, type, version, enabled, uninstalled,
      "peertubeEngine", description,
      homepage,
      settings,
      "createdAt", "updatedAt")
    VALUES ('auth-openid-connect', 1, '0.0.2', 't', 'f',
      '>=2.2.0','Add OpenID connect support to login form in PeerTube.',
      'https://framagit.org/framasoft/peertube/official-plugins/tree/master/peertube-plugin-auth-openid-connect',
      '{"auth-display-name":"LLNG-OIDC","discover-url":"$LEMON_PROTO://$LEMON_HOST/.well-known/openid-configuration","client-id":"$OIDC_CLIENT_ID","client-secret":"$OIDC_SECRET","scope":"$OIDC_SCOPE","username-property":"$OIDC_USERNAME","mail-property":"$OIDC_MAIL","display-name-property":"$OIDC_DN","role-property":""}',
      '$TODAY 00:00:00.000+00', '$TODAY 00:00:00.000+00')
EOF
    fi | PGPASSWORD="$POSTGRES_PASSWORD" \
	    psql -U "$POSTGRES_USER" -h "$POSTGRES_HOST" \
	    -p "$POSTGRES_PORT" "$POSTGRES_DB"
elif test "$OPENLDAP_HOST"; then
    OPENLDAP_BIND_DN_PREFIX="${OPENLDAP_BIND_DN_PREFIX:-cn=peertube,ou=services}"
    OPENLDAP_BIND_PW="${OPENLDAP_BIND_PW:-secret}"
    OPENLDAP_DOMAIN=${OPENLDAP_DOMAIN:-demo.local}
    OPENLDAP_PROTO=${OPENLDAP_PROTO:-ldap}
    if test -z "$OPENLDAP_BASE"; then
	OPENLDAP_BASE=`echo "dc=$OPENLDAP_DOMAIN" | sed 's|\.|,dc=|g'`
    fi
    if test -z "$OPENLDAP_PORT" -a "$OPENLDAP_PROTO" = ldaps; then
	OPENLDAP_PORT=636
    elif test -z "$OPENLDAP_PORT"; then
	OPENLDAP_PORT=389
    fi
    if test "$LEMON_HOST"; then
	if ! test -d /app/storage/plugins/node_modules/peertube-plugin-auth-saml2; then
	    npm run plugin:install -- --npm-name peertube-plugin-auth-saml2
	    SHOULD_RELOAD=true
	fi
	HASID=`echo "SELECT id FROM plugin WHERE name = 'auth-saml2'" | PGPASSWORD="$POSTGRES_PASSWORD" psql -U "$POSTGRES_USER" -h "$POSTGRES_HOST" -p "$POSTGRES_PORT" "$POSTGRES_DB" | awk '/^[ \t]*[1-9]/{print $1}'`
	OPENLDAP_CONF_DN_PREFIX="${OPENLDAP_CONF_DN_PREFIX:-ou=lemonldap,ou=config}"
	SAML_DN=${SAML_DN:-}
	SAML_MAIL=${SAML_MAIL:-email}
	SAML_USERNAME=${SAML_USERNAME:-preferred_username}
	if test -z "$MY_SVCNAME"; then
	    MY_SVCNAME=$PEERTUBE_DOMAIN
	    MY_PROTO=https
	    MY_PORT=443
	fi
	if test -s /saml/lemon.crt; then
	    cat /saml/lemon.crt >/tmp/llng-svc.crt
	elif ! LDAPTLS_REQCERT=never myldapsearch \
		-D "$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE" \
		-w "$OPENLDAP_BIND_PW" \
		-b "$OPENLDAP_CONF_DN_PREFIX,$OPENLDAP_BASE" \
		-H $OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT/ \
		| grep -A32 '^description:: {samlServicePublicKeySig}' \
		| sed 's|description:: {samlServicePublicKeySig}||' \
		| awk 'BEG{ took=0; }{ if (took != 0) { if ($0 == "" || $0 ~ /description/) { exit 0; }} print $0;took=1; }' \
		>/tmp/llng-svc.crt 2>/dev/null; then
	    mv /tmp/llng-svc.crt /tmp/llng-svc.crt.failed
	    cat <<EOF >&2
WARNING: Failed querying $OPENLDAP_PROTO://$OPENLDAP_HOST for SAML signing certificate
WARNING: may not be able to authenticate SAML users
EOF
	fi
	if test -s /tmp/llng-svc.crt; then
	    if test -s /saml/tls.crt -a -s /saml/tls.key; then
		cat /saml/tls.crt >/tmp/pt-svc.crt
		cat /saml/tls.key >/tmp/pt-svc.key
	    else
		openssl req -nodes -new -x509 -days 3650 \
		    -keyout /tmp/pt-svc.key -out /tmp/pt-svc.crt \
		    -subj "/CN=$PEERTUBE_DOMAIN"
	    fi
	    LEMON_SAML_CRT="$(cat /tmp/llng-svc.crt | tr '\n' '|' | sed 's,|,\\n,g')"
	    SAML_CRT="$(cat /tmp/pt-svc.crt | tr '\n' '|' | sed 's,|,\\n,g')"
	    SAML_KEY="$(cat /tmp/pt-svc.key | tr '\n' '|' | sed 's,|,\\n,g')"
	    rm -f /tmp/llng-svc.crt /tmp/pt-svc.key /tmp/pt-svc.crt
	fi
	if test "$SAML_CRT"; then
	    if test "$HASID"; then
		cat <<EOF
UPDATE plugin SET "updatedAt" = '$TODAY 00:00:00.000+00', settings =
      '{"auth-display-name":"LLNG-SAML","client-id":"peertube","login-url":"$LEMON_PROTO://$LEMON_HOST/saml/singleSignOn","provider-certificate":"$LEMON_SAML_CRT","service-certificate":"$SAML_CRT","service-private-key":"$SAML_KEY","username-property":"$SAML_USERNAME","mail-property":"$SAML_MAIL","display-name-property":"$SAML_DN","role-property":"","sign-get-request":true}'
    WHERE name = 'auth-saml2'
EOF
	    else
		cat <<EOF
INSERT INTO plugin (name, type, version, enabled, uninstalled,
      "peertubeEngine", description,
      homepage,
      settings,
      "createdAt", "updatedAt")
    VALUES ('auth-saml2', 1, '0.0.1', 't', 'f',
      '>=2.2.0','Add SAML 2 support to login form in PeerTube.',
      'https://framagit.org/framasoft/peertube/official-plugins/tree/master/peertube-plugin-auth-saml2',
      '{"auth-display-name":"LLNG-SAML","client-id":"peertube","login-url":"$LEMON_PROTO://$LEMON_HOST/saml/singleSignOn","provider-certificate":"$LEMON_SAML_CRT","service-certificate":"$SAML_CRT","service-private-key":"$SAML_KEY","username-property":"$SAML_USERNAME","mail-property":"$SAML_MAIL","display-name-property":"$SAML_DN","role-property":"","sign-get-request":true}',
      '$TODAY 00:00:00.000+00', '$TODAY 00:00:00.000+00')
EOF
	    fi | PGPASSWORD="$POSTGRES_PASSWORD" \
		    psql -U "$POSTGRES_USER" -h "$POSTGRES_HOST" \
		    -p "$POSTGRES_PORT" "$POSTGRES_DB"
	fi
    else
	if ! test -d /app/storage/plugins/node_modules/peertube-plugin-auth-ldap; then
	    npm run plugin:install -- --npm-name peertube-plugin-auth-ldap
	    SHOULD_RELOAD=true
	fi
	SKIP_VERIFY=true
	if test "$OPENLDAP_PROTO" = ldaps; then
	    if test "$LDAP_CUSTOM_CA_PATH" -a \! -s "$LDAP_CUSTOM_CA_PATH"; then
		LDAP_CUSTOM_CA_PATH=
		OPENLDAP_TLS_VERIFY=false
	    fi
	    if test "$OPENLDAP_TLS_VERIFY" = true; then
		SKIP_VERIFY=false
	    fi
	fi
	HASID=`echo "SELECT id FROM plugin WHERE name = 'auth-ldap'" | PGPASSWORD="$POSTGRES_PASSWORD" psql -U "$POSTGRES_USER" -h "$POSTGRES_HOST" -p "$POSTGRES_PORT" "$POSTGRES_DB" | awk '/^[ \t]*[1-9]/{print $1}'`
	if test "$HASID"; then
	    cat <<EOF
UPDATE plugin SET "updatedAt" = '$TODAY 00:00:00.000+00', settings =
    '{"url":"$OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT","weight":100,"bind-dn":"$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE","custom-ca":"$LDAP_CUSTOM_CA_PATH","search-base":"ou=users,$OPENLDAP_BASE","group-base":"","insecure-tls":$SKIP_VERIFY,"mail-property":"mail","search-filter":"(|(mail={{username}})(uid={{username}}))","group-filter":"(member={{dn}})","bind-credentials":"$OPENLDAP_BIND_PW","username-property":"uid","group-admin":"","group-mod":"","group-user":""}'
    WHERE name = 'auth-ldap'
EOF
	else
	    cat <<EOF
INSERT INTO plugin (name, type, version, enabled, uninstalled,
      "peertubeEngine", description,
      homepage,
      settings,
      "createdAt", "updatedAt")
    VALUES ('auth-ldap', 1, '0.0.3', 't', 'f',
      '>=2.2.0','Add LDAP support to login form in PeerTube.',
      'https://framagit.org/framasoft/peertube/official-plugins/tree/master/peertube-plugin-auth-ldap',
      '{"url":"$OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT","weight":100,"bind-dn":"$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE","custom-ca":"$LDAP_CUSTOM_CA_PATH","search-base":"ou=users,$OPENLDAP_BASE","group-base":"","insecure-tls":$SKIP_VERIFY,"mail-property":"mail","search-filter":"(|(mail={{username}})(uid={{username}}))","group-filter":"(member={{dn}})","bind-credentials":"$OPENLDAP_BIND_PW","username-property":"uid","group-admin":"","group-mod":"","group-user":""}',
      '$TODAY 00:00:00.000+00', '$TODAY 00:00:00.000+00')
EOF
	fi | PGPASSWORD="$POSTGRES_PASSWORD" \
		psql -U "$POSTGRES_USER" -h "$POSTGRES_HOST" \
		-p "$POSTGRES_PORT" "$POSTGRES_DB"
    fi
fi

if ls $NODE_CONFIG_DIR/config.yaml.3* >/dev/null 2>&1; then
    if echo "$PT_VERSION" | grep ^4 >/dev/null; then
	if ! test -s "$NODE_CONFIG_DIR/.pt4-upgrade"; then
	    (
		echo NOTICE: upgrading site to PeerTube 4.x
		node ./dist/scripts/migrations/peertube-4.0.js
	    ) | tee -a "$NODE_CONFIG_DIR/.pt4-upgrade"
	fi
fi

if $SHOULD_RELOAD; then
    if test -z "$DO_NOT_RESTART"; then
	WAIT_BEFORE_RESTART=${WAIT_BEFORE_RESTART:-30}
	if test "$WAIT_BEFORE_RESTART" -ge 0; then
	    sleep $WAIT_BEFORE_RESTART
	fi >/dev/null 2>&1
	find /proc -name cmdline 2>/dev/null | grep -E '/proc/[0-9]*/cmdline' \
	    | awk -F/ '{print $3}' | while read candidate
	    do
		if grep dist/server /proc/$candidate/cmdline >/dev/null 2>&1; then
		    echo NOTICE: Reloading PID $candidate -- container will restart
		    kill -HUP $candidate
		fi
	    done
    else
	echo WARNING: PeerTube needs to be restarted
    fi
fi

echo INFO: done provisioning

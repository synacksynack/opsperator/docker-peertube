#!/bin/sh

if test "`id -u`" -ne 0; then
    if test -s /tmp/peertube-passwd; then
	echo Skipping nsswrapper setup - already initialized
    else
	echo Setting up nsswrapper mapping `id -u` to www-data
	(
	    cat /etc/passwd
	    echo "peertube:x:`id -u`:`id -g`:peertube:/app:/bin/bash"
	) >/tmp/peertube-passwd
	(
	    cat /etc/group
	    echo "peertube:x:`id -g`:"
	) >/tmp/peertube-group
    fi
    export NSS_WRAPPER_PASSWD=/tmp/peertube-passwd
    export NSS_WRAPPER_GROUP=/tmp/peertube-group
    export LD_PRELOAD=/usr/lib/libnss_wrapper.so
fi

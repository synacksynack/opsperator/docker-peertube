# k8s PeerTube

Kubernetes PeerTube image.

Build with:

```
$ make build
```

Test with:

```
$ make run
```

Build in OpenShift:

```
$ make ocbuild
```

Cleanup OpenShift assets:

```
$ make ocpurge
```

Environment variables and volumes
----------------------------------

The image recognizes the following environment variables that you can set during
initialization by passing `-e VAR=VALUE` to the Docker `run` command.

|    Variable name               |    Description                 | Default                                                            |
| :----------------------------- | ------------------------------ | ------------------------------------------------------------------ |
|  `DO_CHAPTERS_PLUGIN`          | Installs PeerTube Chapters     | undef                                                              |
|  `DO_LIVE`                     | PeerTube RTMP listener Toggle  | `true`                                                             |
|  `DO_NOT_RESTART`              | Provisioning Reset Toggle      | undef - provisioning my restart container (when plugin installed)  |
|  `LEMON_HOST`                  | LemonLDAP OpenID/SAML Host     | `auth.$OPENLDAP_DOMAIN`                                            |
|  `LEMON_PROTO`                 | LemonLDAP OpenID/SAML Proto    | `http`                                                             |
|  `LIVE_INSTANCE_MAX`           | Global Max PeerTube Lives      | `-1` (unlimited)                                                   |
|  `LIVE_MAX_DURATION`           | Max Live Duration              | `-1`                                                               |
|  `LIVE_TRANSCODE_THREADS`      | Transcodes Threads Count       | `2`                                                                |
|  `LIVE_USER_MAX`               | Per-User Max Lives             | `-1` (unlimited)                                                   |
|  `MY_PORT`                     | SAML Client ID Port            | `443`                                                              |
|  `MY_PROTO`                    | SAML Client ID Proto           | `https`                                                            |
|  `MY_SVCNAME`                  | SAML Client ID Service Name    | `${PEERTUBE_DOMAIN}`                                               |
|  `OIDC_CLIENT_ID`              | OpenID Client ID               | `peertube`                                                         |
|  `OIDC_DN`                     | OpenID Display Name Attribute  | undef                                                              |
|  `OIDC_MAIL`                   | OpenID Mail Attribute          | `mail`                                                             |
|  `OIDC_SCOPE`                  | OpenID Scope                   | `openid email profile`                                             |
|  `OIDC_SECRET`                 | OpenID Client Secret           | undef                                                              |
|  `OIDC_USERNAME`               | OpenID Username Attribute      | `preferred_username`                                               |
|  `ONLY_TRUST_KUBE_CA`          | Don't trust base image CAs     | `false`, any other value disables ca-certificates CAs              |
|  `OPENLDAP_BASE`               | OpenLDAP Base                  | seds `OPENLDAP_DOMAIN`, `example.com` produces `dc=example,dc=com` |
|  `OPENLDAP_DOMAIN`             | OpenLDAP Domain Name           | undef                                                              |
|  `OPENLDAP_CONF_DN_PREFIX`     | LemonLDAP-NG Conf DN Prefix    | `cn=lemonldap,ou=config` (setting up SAML)                         |
|  `OPENLDAP_HOST`               | OpenLDAP Backend Address       | undef                                                              |
|  `OPENLDAP_PORT`               | OpenLDAP Bind Port             | `389` or `636` depending on `OPENLDAP_PROTO`                       |
|  `OPENLDAP_PROTO`              | OpenLDAP Proto                 | `ldap`                                                             |
|  `PEERTUBE_ALLOW_CONFIG_EDITS` | PeerTube Allow Config Edits    | `false`                                                            |
|  `PEERTUBE_ADMIN_PASSWORD`     | PeerTube Admin Password        | `yayayayayayayayayas3cR3742`                                       |
|  `PEERTUBE_CACHE_MINVIEWS`     | PeerTube Cache Minimum Views   | `10`                                                               |
|  `PEERTUBE_CACHE_SIZE`         | PeerTube Cace Size             | `10GB`                                                             |
|  `PEERTUBE_CACHE_STRATEGY`     | PeerTube Caching Strategy      | `recently-added`                                                   |
|  `PEERTUBE_CACHE_TTLMIN`       | PeerTube Cache Minimum TTL     | `48 hours`                                                         |
|  `PEERTUBE_COC`                | PeerTube Code of Conduct       | `Don't be evil`                                                    |
|  `PEERTUBE_DESC`               | PeerTube Site Description 1    | `Welcome to this PeerTube instance`                                |
|  `PEERTUBE_DESC_SHORT`         | PeerTube Site Description 2    | `PeerTube, a federated (ActivityPub) video streaming platform ...` |
|  `PEERTUBE_DOMAIN`             | PeerTube Site FQDN             | `peertube.demo.$OPENLDAP_DOMAIN`                                   |
|  `PEERTUBE_HAS_NSFW`           | PeerTube Allows NSFW Contents  | `true`                                                             |
|  `PEERTUBE_NAME`               | PeerTube Site Name             | `PeerTube`                                                         |
|  `PEERTUBE_NSFW_POLICY`        | PeerTube NSFW Policy           | `do_not_list`                                                      |
|  `PEERTUBE_TERMS`              | PeerTube Terms                 | `No terms for now`                                                 |
|  `PEERTUBE_TRUST_PROXY`        | PeerTube trusted proxies       | `10.0.0.0/8 192.168.0.0/16 172.16.0.0/12`                          |
|  `POSTGRES_DB`                 | Postgres Database Name         | `peertube`                                                         |
|  `POSTGRES_HOST`               | Postgres Database Host         | `127.0.0.1`                                                        |
|  `POSTGRES_PASSWORD`           | Postgres Database Password     | undef                                                              |
|  `POSTGRES_PORT`               | Postgres Database Port         | `5432`                                                             |
|  `POSTGRES_USER`               | Postgres Database Username     | `peertube`                                                         |
|  `POSTGRES_SSL`                | Postgres Uses SSL              | `false`                                                            |
|  `PUBLIC_PROTO`                | PeerTube Public Proto          | `http`                                                             |
|  `REDIS_DATABASE`              | PeerTube Redis Database        | `1`                                                                |
|  `REDIS_HOST`                  | PeerTube Redis Host            | `127.0.0.1`                                                        |
|  `REDIS_PORT`                  | PeerTube Redis Port            | `6379`                                                             |
|  `RTMP_PORT`                   | PeerTube RTMP Port             | `1935`                                                             |
|  `RTMPS_CERT_FILE`             | PeerTube RTMPS Certificate     | undef, required when `DO_RTMP_TLS=true`                            |
|  `RTMPS_KEY_FILE`              | PeerTube RTMPS Key             | undef, required when `DO_RTMP_TLS=true`                            |
|  `RTMPS_PORT`                  | PeerTube RTMPS Port            | `1936`                                                             |
|  `SAML_DN`                     | SAML Display Name Attribute    | undef                                                              |
|  `SAML_MAIL`                   | SAML Mail Attribute            | `mail`                                                             |
|  `SAML_USERNAME`               | SAML Username Attribute        | `preferred_username`                                               |
|  `SMTP_CA`                     | PeerTube SMTP CA Chain         | `/certs/ca.crt` if present                                         |
|  `SMTP_HOST`                   | PeerTube SMTP Relay            | `127.0.0.1`                                                        |
|  `SMTP_NO_STARTTLS`            | PeerTube SMTP No StartTLS      | `true`                                                             |
|  `SMTP_PORT`                   | PeerTube SMTP Port             | `25`                                                               |
|  `SMTP_TLS`                    | PeerTube SMTP Uses TLS         | `false`, unless `SMTP_PORT` is `465`                               |
|  `REPORT_URI_ADDRESS`          | PeerTube Report-URI Address    | ``                                                                 |
|  `WAIT_BEFORE_RESTART`         | Wait Before Restart            | `30` - waits 30 seconds after loading new plugins then restart     |


You can also set the following mount points by passing the `-v /host:/container`
flag to Docker.

|  Volume mount point  | Description                       |
| :------------------- | --------------------------------- |
|  `/app/config`       | PeerTube persisting configuration |
|  `/app/storage`      | PeerTube persisting data          |
|  `/certs`            | PeerTube Certificate (optional)   |
